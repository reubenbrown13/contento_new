# Contento.New
You need to have Postgres, NodeJs, and yarn installed for this to work.

Postgrest is assumed to have the default port with a username and password of postgres.
You can change these values in the config files, and if you want to change the default username and password values for when this script is executed, you just need to modify the lib.mix.contento.ex file.

To compile your own .ez file use the following command : mix archive.build

To install the compiled file in your elixir archive folder use the following command : mix archive.install filename

The appllication will setup all that you need for a contento website provided you have setup the requirements.